#
# THIS FILE IS GENERATED, DO NOT EDIT
#

################################################################################
#
# Ubuntu checks
#
################################################################################

include:
  - local: '/templates/ubuntu.yml'


stages:
  - ubuntu_container_build
  - ubuntu_check


#
# Common variable definitions
#
.ci-image-ubuntu:
  image: $CI_REGISTRY_IMAGE/container-build-base:2022-02-01.0

.ci-variables-ubuntu:
  variables:
    FDO_DISTRIBUTION_PACKAGES: 'wget curl'
    FDO_DISTRIBUTION_EXEC: 'sh test/script.sh'
    FDO_DISTRIBUTION_VERSION: '21.04'
    FDO_EXPIRES_AFTER: '1h'
    FDO_CBUILD: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/cbuild/sha256-e32ab8935c7b151e3b26e206f0591b0a493bffc0fa8ae3d9994a937abb0676cc/cbuild

.ci-variables-ubuntu@x86_64:
  extends: .ci-variables-ubuntu
  variables:
    FDO_DISTRIBUTION_TAG: fdo-ci-x86_64-$CI_PIPELINE_ID

.ci-variables-ubuntu@aarch64:
  extends: .ci-variables-ubuntu
  variables:
    FDO_DISTRIBUTION_TAG: fdo-ci-aarch64-$CI_PIPELINE_ID

#
# A few templates to avoid writing the image and stage in each job
#
.ubuntu:ci@container-build@x86_64:
  extends:
    - .fdo.container-build@ubuntu
    - .ci-variables-ubuntu@x86_64
    - .ci-image-ubuntu
  stage: ubuntu_container_build


.ubuntu:ci@container-build@aarch64:
  extends:
    - .fdo.container-build@ubuntu
    - .ci-variables-ubuntu@aarch64
    - .ci-image-ubuntu
  tags:
    - aarch64
  stage: ubuntu_container_build


#
# Qemu build
#
.ubuntu:ci@qemu-build@x86_64:
  extends:
    - .fdo.qemu-build@ubuntu@x86_64
    - .ci-variables-ubuntu@x86_64
    - .ci-image-ubuntu
  image: $CI_REGISTRY_IMAGE/qemu-build-base:2022-02-01.0
  stage: ubuntu_container_build
  artifacts:
    name: logs-$CI_PIPELINE_ID
    when: always
    expire_in: 1 week
    paths:
      - console.out

#
# generic ubuntu checks
#
.ubuntu@check@x86_64:
  extends:
    - .ci-variables-ubuntu@x86_64
    - .fdo.distribution-image@ubuntu
  stage: ubuntu_check
  script:
      # run both curl and wget because one of those two is installed and one is
      # in the base image, but it depends on the distro which one
    - curl --insecure https://gitlab.freedesktop.org
    - wget --no-check-certificate https://gitlab.freedesktop.org
      # make sure our test script has been run
    - if [[ -e /test_file ]] ;
      then
        echo $FDO_DISTRIBUTION_EXEC properly run ;
      else
        exit 1 ;
      fi


.ubuntu@qemu-check@x86_64:
  stage: ubuntu_check
  extends:
    - .ci-variables-ubuntu@x86_64
    - .fdo.distribution-image@ubuntu
  tags:
    - kvm
  script:
    - pushd /app
      # start the VM
    - /app/vmctl start
      # run both curl and wget because one of those two is installed and one is
      # in the base image, but it depends on the distro which one
    - /app/vmctl exec curl --insecure https://gitlab.freedesktop.org
    - /app/vmctl exec wget --no-check-certificate https://gitlab.freedesktop.org
      # terminate the VM
    - /app/vmctl stop

      # start the VM, with the kernel parameters (if we have a kernel)
    - |
      [[ $(ls /app/vmlinuz*) ]] || exit 0
    - /app/vmctl start-kernel
      # make sure we can still use curl/wget
    - /app/vmctl exec curl --insecure https://gitlab.freedesktop.org
    - /app/vmctl exec wget --no-check-certificate https://gitlab.freedesktop.org
      # terminate the VM
    - /app/vmctl stop
  artifacts:
    name: logs-$CI_PIPELINE_ID
    when: always
    expire_in: 1 week
    paths:
      - console.out


#
# straight ubuntu build and test
#
ubuntu:21.04@container-build@x86_64:
  extends: .ubuntu:ci@container-build@x86_64


ubuntu:21.04@check@x86_64:
  extends: .ubuntu@check@x86_64
  needs:
    - ubuntu:21.04@container-build@x86_64

# Test FDO_BASE_IMAGE. We don't need to do much here, if our
# FDO_DISTRIBUTION_EXEC script can run curl+wget this means we're running on
# the desired base image. That's good enough.
ubuntu:21.04@base-image@x86_64:
  extends: ubuntu:21.04@container-build@x86_64
  stage: ubuntu_check
  variables:
    # We need to duplicate FDO_DISTRIBUTION_TAG here, gitlab doesn't allow nested expansion
    FDO_BASE_IMAGE: registry.freedesktop.org/$CI_PROJECT_PATH/ubuntu/21.04:fdo-ci-x86_64-$CI_PIPELINE_ID
    FDO_DISTRIBUTION_PACKAGES: ''
    FDO_DISTRIBUTION_EXEC: 'test/test-wget-curl.sh'
    FDO_FORCE_REBUILD: 1
    FDO_DISTRIBUTION_TAG: fdo-ci-baseimage-x86_64-$CI_PIPELINE_ID
  needs:
    - ubuntu:21.04@container-build@x86_64

#
# /cache ubuntu check (in build stage)
#
# Also ensures setting FDO_FORCE_REBUILD will do the correct job
#
ubuntu@cache-container-build@x86_64:
  extends: .ubuntu:ci@container-build@x86_64
  before_script:
      # The template normally symlinks the /cache
      # folder, but we want a fresh new one for the
      # tests.
    - mkdir runner_cache_$CI_PIPELINE_ID
    - uname -a | tee runner_cache_$CI_PIPELINE_ID/foo-$CI_PIPELINE_ID

  artifacts:
    paths:
      - runner_cache_$CI_PIPELINE_ID/*
    expire_in: 1 week

  variables:
    FDO_DISTRIBUTION_TAG: fdo-ci-cache-x86_64-$CI_PIPELINE_ID
    FDO_DISTRIBUTION_EXEC: 'bash test/test_cache.sh $CI_PIPELINE_ID'
    FDO_CACHE_DIR: $CI_PROJECT_DIR/runner_cache_$CI_PIPELINE_ID
    FDO_FORCE_REBUILD: 1

#
# /cache ubuntu check (in check stage)
#
ubuntu@cache-check@x86_64:
  stage: ubuntu_check
  image: alpine:latest
  script:
    # in the previous stage (ubuntu@cache-container-build@x86_64),
    # test/test_cache.sh checked for the existance of `/cache/foo-$CI_PIPELINE_ID`
    # and if it found it, it wrote `/cache/bar-$CI_PIPELINE_ID`.
    #
    # So if we have in the artifacts `bar-$CI_PIPELINE_ID`, that means
    # 2 things:
    # - /cache was properly mounted while building the container
    # - the $FDO_CACHE_DIR has been properly written from within the
    #   building container, meaning the /cache folder has been successfully
    #   updated.
    - if [ -e $CI_PROJECT_DIR/runner_cache_$CI_PIPELINE_ID/bar-$CI_PIPELINE_ID ] ;
      then
        echo Successfully read/wrote the cache folder, all good ;
      else
        echo FAILURE while retrieving the previous artifacts ;
        exit 1 ;
      fi
  needs:
    - job: ubuntu@cache-container-build@x86_64
      artifacts: true


ubuntu:21.04@container-build@aarch64:
  extends: .ubuntu:ci@container-build@aarch64
  variables:
    FDO_DISTRIBUTION_TAG: fdo-ci-aarch64-$CI_PIPELINE_ID


ubuntu:21.04@check@aarch64:
  extends: .ubuntu@check@x86_64
  tags:
    - aarch64
  variables:
    FDO_DISTRIBUTION_TAG: fdo-ci-aarch64-$CI_PIPELINE_ID
  needs:
    - ubuntu:21.04@container-build@aarch64


#
# make sure we do not rebuild the image if the tag exists (during the check)
#
do not rebuild ubuntu:21.04@container-build@x86_64:
  extends: .ubuntu:ci@container-build@x86_64
  stage: ubuntu_check
  variables:
    FDO_UPSTREAM_REPO: $CI_PROJECT_PATH
    FDO_DISTRIBUTION_PACKAGES: 'this-package-should-not-exist'
  needs:
    - ubuntu:21.04@container-build@x86_64


#
# check if the labels were correctly applied
#
check labels ubuntu@x86_64:21.04:
  extends:
    - ubuntu:21.04@check@x86_64
  image: $CI_REGISTRY_IMAGE/container-build-base:2022-02-01.0
  script:
    # FDO_DISTRIBUTION_IMAGE still has indirections
    - DISTRO_IMAGE=$(eval echo ${FDO_DISTRIBUTION_IMAGE})

    # retrieve the infos from the registry (once)
    - JSON_IMAGE=$(skopeo inspect docker://$DISTRO_IMAGE)

    # parse all the labels we care about
    - IMAGE_PIPELINE_ID=$(echo $JSON_IMAGE | jq -r '.Labels["fdo.pipeline_id"]')
    - IMAGE_JOB_ID=$(echo $JSON_IMAGE | jq -r '.Labels["fdo.job_id"]')
    - IMAGE_PROJECT=$(echo $JSON_IMAGE | jq -r '.Labels["fdo.project"]')
    - IMAGE_COMMIT=$(echo $JSON_IMAGE | jq -r '.Labels["fdo.commit"]')

    # some debug information
    - echo $JSON_IMAGE
    - echo $IMAGE_PIPELINE_ID $CI_PIPELINE_ID
    - echo $IMAGE_JOB_ID
    - echo $IMAGE_PROJECT $CI_PROJECT_PATH
    - echo $IMAGE_COMMIT $CI_COMMIT_SHA

    # ensure the labels are correct (we are on the same pipeline)
    - '[[ x"$IMAGE_PIPELINE_ID" == x"$CI_PIPELINE_ID" ]]'
    - '[[ x"$IMAGE_JOB_ID" != x"" ]]' # we don't know the job ID, but it must be set
    - '[[ x"$IMAGE_PROJECT" == x"$CI_PROJECT_PATH" ]]'
    - '[[ x"$IMAGE_COMMIT" == x"$CI_COMMIT_SHA" ]]'
  needs:
    - ubuntu:21.04@container-build@x86_64


#
# make sure we do not rebuild the image if the tag exists in the upstream
# repository (during the check)
# special case where FDO_REPO_SUFFIX == ci_templates_test_upstream
#
pull upstream ubuntu:21.04@container-build@x86_64:
  extends: .ubuntu:ci@container-build@x86_64
  stage: ubuntu_check
  variables:
    FDO_UPSTREAM_REPO: $CI_PROJECT_PATH
    FDO_REPO_SUFFIX: ubuntu/ci_templates_test_upstream
    FDO_DISTRIBUTION_PACKAGES: 'this-package-should-not-exist'
  needs:
    - ubuntu:21.04@container-build@x86_64

#
# Try our ubuntu scripts with other versions and check
#

ubuntu:20.04@container-build@x86_64:
  extends: .ubuntu:ci@container-build@x86_64
  variables:
    FDO_DISTRIBUTION_VERSION: '20.04'

ubuntu:20.04@check@x86_64:
  extends: .ubuntu@check@x86_64
  variables:
    FDO_DISTRIBUTION_VERSION: '20.04'
  needs:
    - ubuntu:20.04@container-build@x86_64

ubuntu:21.04@qemu-build@x86_64:
  extends: .ubuntu:ci@qemu-build@x86_64
  variables:
    FDO_DISTRIBUTION_TAG: fdo-ci-qemu-x86_64-$CI_PIPELINE_ID
    FDO_DISTRIBUTION_PACKAGES: 'wget curl'
    FDO_CI_TEMPLATES_QEMU_BASE_IMAGE: $CI_REGISTRY_IMAGE/qemu-base:2022-02-01.0


ubuntu:21.04@qemu-check@x86_64:
  extends: .ubuntu@qemu-check@x86_64
  variables:
    FDO_DISTRIBUTION_TAG: fdo-ci-qemu-x86_64-$CI_PIPELINE_ID
  needs:
    - ubuntu:21.04@qemu-build@x86_64
