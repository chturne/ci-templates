# fd.o CI templates

This repository contains a set of .gitlab-ci.yml templates that can be used
to easily maintain a complex set of CI requirements. Specifically, the
templates simplify custom building and managing images for various
distributions. For example, with the CI Templates the job to build a Fedora
32 image looks like this:

```
build-a-f32:
  extends: .fdo.container-build@fedora@x86_64
  variables:
    FDO_DISTRIBUTION_VERSION: '32'
    FDO_DISTRIBUTION_PACKAGES: 'wget curl'
    FDO_DISTRIBUTION_TAG: 'built-on-20200321'
```

The above snippet installs ``wget`` and ``curl`` on the Fedora 32 base image
and tags it into the local image repository. Other jobs can easily use that
image, for example like this:

```
test-on-f32:
  extends: .fdo.distribution-image@fedora
  script:
    - make check
    - other test commands
  variables:
    FDO_DISTRIBUTION_VERSION: '32'
    FDO_DISTRIBUTION_TAG: 'built-on-20200321'
```

More examples and details are available in the 
[CI Templates
documentation](https://freedesktop.pages.freedesktop.org/ci-templates).


Current projects using this are (non exhaustive list):
- https://gitlab.freedesktop.org/xorg/xserver
- https://gitlab.freedesktop.org/libinput/libinput
- https://gitlab.freedesktop.org/libevdev/libevdev
- https://gitlab.freedesktop.org/libevdev/python-libevdev
- https://gitlab.freedesktop.org/wayland/weston
- https://gitlab.freedesktop.org/mesa/mesa
- https://gitlab.freedesktop.org/NetworkManager/NetworkManager

# Documentation

See https://freedesktop.pages.freedesktop.org/ci-templates

# Contributing

See https://freedesktop.pages.freedesktop.org/ci-templates/hacking.html

# License

The CI templates are licensed under the MIT license.
